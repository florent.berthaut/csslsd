<p align="center"><img width="150" alt="CSS LSD Logo" src="./ressources/logo.svg"></p>
<p align="center">CssLsd is a Chromium Extension which allows for interactively modifying the style of web pages using MIDI messages, Keyboard presses or Audio input features.</p>

# CSSLSD Documentation
Table of contents

- [CSSLSD Documentation](#csslsd-documentation)
  - [Setup](#setup)
    - [Browser compatibility](#browser-compatibility)
      - [AudioWorklet (audio sound analyzer)](#audioworklet-audio-sound-analyzer)
      - [MIDI API](#midi-api)
  - [Usage](#usage)
    - [User Interface](#user-interface)
      - [Open extension](#open-extension)
        - [Popup](#popup)
        - [Devtool & deported devtool](#devtool--deported-devtool)
    - [CSSLSD Code](#csslsd-code)
    - [MIDI Variables](#midi-variables)
    - [Audio Variables](#audio-variables)
    - [Types](#types)
    - [Options](#options)
    - [Start to use functionnalities](#start-to-use-functionnalities)
      - [Audio](#audio)
      - [MIDI](#midi)
    - [Loops](#loops)
    - [Upload and download](#upload-and-download)
  - [Examples](#examples)
  - [Ressources](#ressources)

## Setup

- Download or Clone CssLsd
- Start Chromium / Chrome / Brave / Edge
- Type `chrome://extensions` in the address bar
- Click "Load unpacked extension" and select the csslsd folder
- Now the extension should appear in the menu bar

### Browser compatibility 

#### AudioWorklet (audio sound analyzer)

| Chrome | Edge | Firefox | Opéra | Brave |
|--------|------|---------|-------|-------|
| 64     | 79   | 76      | 51    | 64    |

#### MIDI API

| Chrome | Edge | Firefox | Opéra | Brave |
|--------|------|---------|-------|-------|
| 43     | 79   | NO      | 30    | 43    |

## Usage

### User Interface

#### Open extension

##### Popup

Pin the extension to get it close to URL bar. CLick on CSSLSD logo to open it.

![Popup view](doc/popup.png)

##### Devtool & deported devtool

Open devtool, in "element", "source", "console" bar click on view more arrow, then drag-and-drop "CSSLSD" button.
You can put "element" tab to bottom by right-clicking on it.
For deported devtool, click on more (top-right) and then click on separate devtool dock.

![Devtool view](doc/devtool.gif)

### CSSLSD Code

You need to use a custom CSS template, like below to map your audio or MIDI events to some CSSLSD properties. All parameters are between **square brackets**. You can use multiple parameters in a line. You can also type pure CSS in code editor, this CSS wil be apply on code auto-save (each time your type something in editor).

You can extend parameter by heritage with template `[<parameter> <type> <option> <audio or midi control>]`. This is not mandatory to type all heritage, but you need to restect the direction. This is allowed : `[<parameter> <type> <option>]` or `[<parameter> <type>]`. This is **not** allowed : `[<parameter> <audio or midi control>]`.

You can use without CSSLSD code without any parameters, use same template like above. How to use : `[<type> <option> <audio or midi control>]`.

Pseudo elements like `::after` or `::before` are working, if element have already a pseudo element add your CSS LSD template otherwise add pure CSS, it will create the pseudo element by adding style with class `CSSLSD` at the end of the document. For this pure CSS add : `position:absolute; content:''; width:10px; height:10px;`.

Code example:

```css
body {
    /* Tip : You can use comments */
    /* Here, we map "couleur" parameter to background-color property extended with a new range between black (hex) and white (hex). */
    background-color:[couleur #000:#fff];
}
h1 {
    /* Simple parameter usage */
    opacity: [opacité];
    font-size:[0:20 d loud];
    color: #000000;
}
```

Parameters example:

<p align="center">
<img width="600" alt="Settings" src="./doc/settings.png">
</p>

To ***save*** your template, it's automatic ! Just type, edit what you want. It will be saved.

### MIDI Variables

With WebMidiAPI all events will be monitored. Note is when you push a button or a keyboard key, control change is like a fader or potentiometer change.

| Midi Event  | Parameter | Informations                                        |
|-------------|-----------|-----------------------------------------------------|
| note        | id        | Note on, use case : note:21                         |
| cc          | id        | Control change, use case : cc:10                    |
| pb          |           | Pitchbend, use case : pb                            |

### Audio Variables

When audio is on, some audio events are mapped on variables bellow. AudioWorklet is used to get loud information. All others parameters are based on frequency analysing. Energy will output the average of a range of frequency. Onset is an audio correlate, to find to wich note the frequency is similar. Attack is analysing changes on loud parameter and if the audio loud is upscaling too much this event will take  place.

| Audio Event | Parameter | Informations                                        |
|-------------|-----------|-----------------------------------------------------|
| low         |           | low energy (20-250Hz), use case : low               |
| lomi        |           | lomi energy (250-500Hz), use case : lomi            |
| mid         |           | mid energy (500-2e3Hz), use case : mid              |
| mihi        |           | mihi energy (2e3-4e3Hz), use case : mihi            |
| hi          |           | High energy (4e3-16e3Hz), use case : hi             |
| loud        |           | Volume meter, use case : loud                       |
| onset       | note      | Note, use case : onset:A                            |
| attack      |           | Attack, use case : attack                           |

### Types

| Option      | Informations                                                    |
|-------------|-----------------------------------------------------------------|
| min:max     | Decimal or hexadecimal min and max splited by ":"               |
| A;B;C       | Array splited by ";"                                            |

### Options

| Option      | Informations                                                    |
|-------------|-----------------------------------------------------------------|
| r           | Random value between type object                                |
| i:1         | Incremental value between type object, increment can be negative|
| d           | Linear value between type object                                |

### Start to use functionnalities

#### Audio

Click on activate button, right top. When it becomes green you can click on audio one. If audio well started it's become green too. Don't forget to authorize the tab to get access to your microphone.

![Devtool view](doc/activate_audio.gif)

#### MIDI

Click on activate button, then on MIDI button. If this one becomes green, the MIDI is operationnal to use. Your device is displayed on top left. On control change on your MIDI device and if the extension pop-up is open the control ID will be displayed on top center. And if you focus text MIDI column parameter, control will be add to the list.

### Loops

When you press record all interactions with the midi device are recorded in a table. When finished recording, press stop. You can then play your loop by pressing play or re-record over the previously created loop. Finally, you can overwrite the created loop. You have 4 slots available to save your loops. You can also map MIDI events to the actions listed above.
You can't record simultaneously loops, push on record N°X button will overwrite the loop N°X.

### Upload and download

You have the possibility to export all CSS-LSD settings and codes as a JSON file. You can then import a previously exported JSON file from the extension. This can be useful for saving presets according to websites.

## Examples

This exemple show, how to duplicate parameter, to add "opacité" one. And then add it to CSSLSD code via auto-completition.

![Devtool view](doc/parameter_opacity.gif)

## Ressources

- Chrome extensions : <https://developer.chrome.com/docs/extensions/mv3/getstarted/>
- ACE.js : <https://ace.c9.io/>
- WebMIDI : <https://webmidijs.org/>
- CSSParser : <http://glazman.org/JSCSSP/index.html>
