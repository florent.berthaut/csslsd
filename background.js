let activate = false;
let css = `body {
	background-color: [couleur];
}
h1, h2 {
	/* opacity: 1; */
	opacity: [opacité];
	font-family: [typo];
	font-size: [chiffre2 20:100]px;
	color: rgb([chiffre 0:255],255,255);
}`;
let parameters = `[[\"       \",\"typo\",\"sans-serif;serif;cursive;fantasy\",\"r\",\"note:10\",\"onset:E\"],[\"       \",\"chiffre\",\"0:1000\",\"i:100\",\"cc:60\",\"onset:D\"],[\"       \",\"chiffre2\",\"0:200\",\"d\",\"cc:60\",\"loud\"],[\"       \",\"couleur\",\"#000:#fff\",\"r\",\"cc:60\",\"onset:E\"],[\"       \",\"opacité\",\"0:1\",\"d\",\"cc:1\",\"hi\"]]`;
let all = "";
let audioI = false;
let midiI = false;
let popup = false;
let loopPlay = false;
let sensibility = 0.95;
let records = {
	loop: [
		[
			{
				time: 1,
				data: { value: 0, controller: { number: 1 } },
				type: 'controlchange'
			},
			{
				time: 2000,
				data: { value: 1, controller: { number: 1 } },
				type: 'controlchange'
			},
			{
				time: 3000,
				data: { value: 0, controller: { number: 1 } },
				type: 'controlchange'
			}
		],
		[],
		[],
		[]
	],
	info: [
		{ isLoop: false, isRecording: false, controllerPlayPause: '', controllerRecord: '' },
		{ isLoop: false, isRecording: false, controllerPlayPause: '', controllerRecord: '' },
		{ isLoop: false, isRecording: false, controllerPlayPause: '', controllerRecord: '' },
		{ isLoop: false, isRecording: false, controllerPlayPause: '', controllerRecord: '' }
	]
};
let debug = false;

//Initialize the CSS storage on startup
chrome.runtime.onInstalled.addListener(() => {
	chrome.storage.sync.set({ activate });
	chrome.storage.local.set({ css });
	chrome.storage.local.set({ parameters });
	chrome.storage.local.set({ all });
	chrome.storage.sync.set({ audioI });
	chrome.storage.sync.set({ midiI });
	chrome.storage.sync.set({ popup });
	chrome.storage.sync.set({ loopPlay });
	chrome.storage.local.set({ records });
	chrome.storage.sync.set({ debug });
	chrome.storage.sync.set({ sensibility });
});


/**
 * @description This function is called when popup is opened and add a listener when user close it.
 */
chrome.runtime.onConnect.addListener(function (port) {
	if (port.name === "popup") {
		port.onDisconnect.addListener(function () {
			chrome.storage.sync.set({ popup: false });
		});
	}
});