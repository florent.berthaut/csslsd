var audioButton = false;
var midiButton = false;
var activateButton;
var dataMidi;
var editor;
var langTools;

var trashIcon = `<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-trash" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
<path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
<line x1="4" y1="7" x2="20" y2="7"></line>
<line x1="10" y1="11" x2="10" y2="17"></line>
<line x1="14" y1="11" x2="14" y2="17"></line>
<path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12"></path>
<path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3"></path>
</svg>`;
var recordIcon = `<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-player-record" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
<path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
<circle cx="12" cy="12" r="7"></circle>
</svg>`;
var stopIcon = `<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-player-stop" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
<path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
<rect x="5" y="5" width="14" height="14" rx="2"></rect>
</svg>`;
var pauseIcon = `<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-player-pause" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
<path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
<rect x="6" y="5" width="4" height="14" rx="1"></rect>
<rect x="14" y="5" width="4" height="14" rx="1"></rect>
</svg>`;
var playIcon = `<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-player-play" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
<path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
<path d="M7 4v16l13 -8z"></path>
</svg>`;
var minus = "<svg xmlns=\"http://www.w3.org/2000/svg\" class=\"icon icon-tabler icon-tabler-minus\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" stroke-width=\"1.5\" stroke=\"currentColor\" fill=\"none\" stroke-linecap=\"round\" stroke-linejoin=\"round\"><path stroke=\"none\" d=\"M0 0h24v24H0z\" fill=\"none\"></path><circle cx=\"12\" cy=\"12\" r=\"9\"></circle><line x1=\"9\" y1=\"12\" x2=\"15\" y2=\"12\"></line></svg>";
var plus = "<svg xmlns=\"http://www.w3.org/2000/svg\" class=\"icon icon-tabler icon-tabler-plus\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" stroke-width=\"1.5\" stroke=\"currentColor\" fill=\"none\" stroke-linecap=\"round\" stroke-linejoin=\"round\"><path stroke=\"none\" d=\"M0 0h24v24H0z\" fill=\"none\"></path><circle cx=\"12\" cy=\"12\" r=\"9\"></circle><line x1=\"9\" y1=\"12\" x2=\"15\" y2=\"12\"></line><line x1=\"12\" y1=\"9\" x2=\"12\" y2=\"15\"></line></svg>";
var icon = "<svg xmlns=\"http://www.w3.org/2000/svg\" class=\"icon icon-tabler icon-tabler-grip-vertical\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" stroke-width=\"1.5\" stroke=\"currentColor\" fill=\"none\" stroke-linecap=\"round\" stroke-linejoin=\"round\"> <path stroke=\"none\" d=\"M0 0h24v24H0z\" fill=\"none\"></path> <circle cx=\"9\" cy=\"5\" r=\"1\"></circle> <circle cx=\"9\" cy=\"12\" r=\"1\"></circle> <circle cx=\"9\" cy=\"19\" r=\"1\"></circle> <circle cx=\"15\" cy=\"5\" r=\"1\"></circle> <circle cx=\"15\" cy=\"12\" r=\"1\"></circle> <circle cx=\"15\" cy=\"19\" r=\"1\"></circle></svg>";

function setAutocompleteParameters() {
	var autoComplete = [];
	chrome.storage.local.get(['parameters'], function (result) {
		parameters = JSON.parse(result.parameters);
		parameters.forEach(p => {
			autoComplete.push({ value: '[' + p[1] + ']', score: 1, meta: p[3] + " " + p[4] });
		});
	});
	const cssTemplate = {
		getCompletions: (editor, session, pos, prefix, callback) => {
			callback(null, autoComplete);
		},
	};
	langTools.addCompleter(cssTemplate);
}

/**
 * Load the editor and add css
 */
function loadEditor() {
	editor = ace.edit("editor", {
		autoScrollEditorIntoView: true,
		maxLines: 40,
		minLines: 2
	});
	editor.setTheme("ace/theme/tomorrow_night_blue");
	editor.session.setMode("ace/mode/css");
	editor.getSession().setUseWorker(false); // Disable syntax checking, not working in chrome extension V3
	langTools = ace.require('ace/ext/language_tools'); // Add autocomplete
	langTools.setCompleters([langTools.snippetCompleter, langTools.keyWordCompleter]);
	editor.setOptions({
		enableBasicAutocompletion: true,
		enableSnippets: true,
		enableLiveAutocompletion: true
	});
	chrome.storage.local.get(['css'], function (result) {
		editor.getSession().setValue(result.css)
	});
	setAutocompleteParameters();
	editor.getSession().on('change', function () {
		updateCss();
	});
}



/**
 * The function is called when the extension is loaded. 
 * It gets the current values of the settings from the browser's local storage. 
 * It then sets the texts of the settings buttons to the current values of the settings.
 */
function initUi() {
	chrome.storage.sync.get(['activate'], function (result) {
		activateButton = result.activate;
		if (activateButton) {
			$('#onOff').addClass('active');
		} else {
			$('#onOff').removeClass('active');
		}
	});
	chrome.storage.sync.get(['audioI'], function (result) {
		audioButton = result.audioI;
		if (result.audioI) {
			$('.audioI').addClass('active');
		}
		else {
			$('.audioI').removeClass('active');
		}
	});
	chrome.storage.sync.get(['midiI'], function (result) {
		midiButton = result.midiI;
		if (result.midiI) {
			$('.midiI').addClass('active');
		}
		else {
			$('.midiI').removeClass('active');
		}
	});
	chrome.storage.sync.get(['sensibility'], function (result) {
		$('#sensibility').val(result.sensibility);
	});
	chrome.storage.local.get(['records'], function (result) {
		$('#loopList').text('');
		for (let i = 0; i < result.records.loop.length; i++) {
			if (result.records.loop[i].length > 0) {
				$('#loopList').append('<tr><td>Boucle n°' + (i + 1) + '</td><td>' + result.records.loop[i][result.records.loop[i].length - 1].time + 'ms</td><td>' + result.records.loop[i].length + '</td><td><button id="record" value="' + i + '">' + (result.records.info[i].isRecording ? stopIcon : recordIcon) + '</button><a>note:<span contenteditable class="spanEditable"></span></a><button id="loopPlay" value="' + i + '">' + (result.records.info[i].isLoop ? pauseIcon : playIcon) + '</button><a>note:<span contenteditable class="spanEditable"></span></a></td><td><button id="loopReset" value="' + i + '">' + trashIcon + '</button></td></tr>');
			}
			else {
				$('#loopList').append('<tr><td>Boucle n°' + (i + 1) + '</td><td></td><td>0</td><td><button id="record" value="' + i + '">' + recordIcon + '</button><a>note:<span contenteditable class="spanEditable"></span></a><button id="loopPlay" value="' + i + '">' + playIcon + '</button><a>note:<span contenteditable class="spanEditable"></span></a></td><td><button id="loopReset" value="' + i + '">' + trashIcon + '</button></td></tr>');
			}
		}
	});
	chrome.storage.local.get(['parameters'], function (result) {
		parameters = JSON.parse(result.parameters);
		var tbl_body = "";
		parameters.forEach(p => {
			var tbl_row = "";
			for (let index = 1; index < p.length; index++) {
				tbl_row += "<td contenteditable>" + p[index] + "</td>";
			}
			tbl_body += "<tr><td>" + icon + plus + minus + "</td>" + tbl_row + "</tr>";
		});
		$("#parameters").html(tbl_body);
	});
	$(function () {
		$("#parameters").sortable({
			// revert: true,
			axis: "y",
			containment: "parent",
			cancel: ':input,button,[contenteditable]'
		});
	});
}
$(document).on('input', '#loopList span', function () {
	spans = $('#loopList span');
	chrome.storage.local.get(['records'], function (result) {
		records = result.records;
		row = 0;
		for (let i = 0; i < 4; i++) {
			row = i * 2;
			records.info[i].controllerPlayPause = spans[row].textContent;
			records.info[i].controllerRecord = spans[row + 1].textContent;
		}
		chrome.storage.local.set({ 'records': records });
	});
});

$(document).on('click', '#doc', function () {
	chrome.tabs.create({ 'url': "/README.html" });
});
$(document).on('click', '#debug', function () {
	sendMessage({ type: 'debug', data: false });
});

var allParameters;
document.getElementById("parameters").addEventListener("input", function () {
	saveParameters();
}, false);

/**
 * @description Save the parameters, transform it into JSON and add auto-completion.
 */
function saveParameters() {
	var tbl = $('#parameters tr').get().map(function (row) {
		return $(row).find('td').get().map(function (cell) {
			return $(cell).text();
		});
	});
	allParameters = JSON.stringify(tbl);
	chrome.storage.local.set({ parameters: JSON.stringify(tbl) });
	updateCss();
	setAutocompleteParameters();
}

/**
 * @description This function is called on auto save, it updates the events with the current values of css and parameters.
 * For each css line, we check if line is connected to a parameter. If it is, we update the event with the current value of the parameter and verify heritage.
 * If line has no parameter but have CSS LSD template we update the event with the current value of the template.
 * If line has no parameter and no CSS LSD template we update the event with the current value of the css as type 'pure'.
 */
function analyseAll() {
	var isParameter = false;
	chrome.storage.local.get(['parameters', 'css', 'debug'], function (result) {
		var all = [];
		var parser = new CSSParser();
		var sheet = parser.parse(result.css, false, false);
		if (sheet !== null && sheet.cssRules.length > 0) {
			sheet.cssRules.forEach(element => {
				if (element !== null && element.declarations.length > 0) {
					element.declarations.forEach(css => {
						var templates = [];
						cssValue = css.valueText;
						count = css.valueText.split("[").length;
						if (count > 1) {
							for (let index = 0; index < count - 1; index++) {
								isParameter = false;
								template = cssValue.substring(
									cssValue.indexOf("[") + 1,
									cssValue.indexOf("]")
								);
								cssValue = cssValue.replace("[" + template + "]", "");
								var parameters = JSON.parse(result.parameters);
								parameters.forEach(p => {
									templateSplited = template.split(' ');
									if (p[1] == templateSplited[0]) {
										isParameter = true;
										if (p[4] != "") {
											p[4].split(' ').forEach(midiE => {
												var obj = {
													type: '',
													name: templateSplited[0],
													templateFull: template,
													eventType: templateSplited.length > 3 ? (templateSplited[3].includes(':') ? templateSplited[3].split(':')[0] : templateSplited[3]) : '',
													eventName: templateSplited.length > 3 ? (templateSplited[3].includes(':') ? templateSplited[3].split(':')[1] : '') : '',
													options: templateSplited.length > 2 ? (templateSplited[2]) : '',
													array: templateSplited.length > 1 ? (templateSplited[1].includes(';') ? templateSplited[1].split(';') : '') : '',
													min: templateSplited.length > 1 ? (templateSplited[1].includes(':') ? templateSplited[1].split(':')[0] : '') : '',
													max: templateSplited.length > 1 ? (templateSplited[1].includes(':') ? templateSplited[1].split(':')[1] : '') : '',
												}
												obj.type = 'midi';
												if (midiE.includes(':')) {
													obj.eventType = ((obj.eventType == '') ? midiE.split(':')[0] : obj.eventType);
													obj.eventName = ((obj.eventName == '') ? midiE.split(':')[1] : obj.eventName);
												}
												else {
													obj.eventType = ((obj.eventType == '') ? midiE : obj.eventType);
													obj.eventName = ((obj.eventName == '') ? '' : obj.eventName);
												}
												obj.options = ((obj.options == '') ? (p[3]) : obj.options);
												obj.array = ((obj.array == '') ? (p[2].includes(';') ? p[2].split(';') : '') : obj.array);
												obj.min = ((obj.min == '') ? (p[2].includes(':') ? p[2].split(':')[0] : 0) : obj.min);
												obj.max = ((obj.max == '') ? (p[2].includes(':') ? p[2].split(':')[1] : 1) : obj.max);
												templates.push(obj);
											});
										}
										if (p[5] != "") {
											p[5].split(' ').forEach(audioE => {
												var obj = {
													type: '',
													name: templateSplited[0],
													templateFull: template,
													eventType: templateSplited.length > 3 ? (templateSplited[3].includes(':') ? templateSplited[3].split(':')[0] : templateSplited[3]) : '',
													eventName: templateSplited.length > 3 ? (templateSplited[3].includes(':') ? templateSplited[3].split(':')[1] : '') : '',
													options: templateSplited.length > 2 ? (templateSplited[2]) : '',
													array: templateSplited.length > 1 ? (templateSplited[1].includes(';') ? templateSplited[1].split(';') : '') : '',
													min: templateSplited.length > 1 ? (templateSplited[1].includes(':') ? templateSplited[1].split(':')[0] : '') : '',
													max: templateSplited.length > 1 ? (templateSplited[1].includes(':') ? templateSplited[1].split(':')[1] : '') : '',
												}
												obj.type = 'audio';
												if (audioE.includes(':')) {
													obj.eventType = ((obj.eventType == '') ? audioE.split(':')[0] : obj.eventType);
													obj.eventName = ((obj.eventName == '') ? audioE.split(':')[1] : obj.eventName);
												}
												else {
													obj.eventType = ((obj.eventType == '') ? audioE : obj.eventType);
													obj.eventName = ((obj.eventName == '') ? '' : obj.eventName);
												}
												obj.options = ((obj.options == '') ? (p[3]) : obj.options);
												obj.array = ((obj.array == '') ? (p[2].includes(';') ? p[2].split(';') : '') : obj.array);
												obj.min = ((obj.min == '') ? (p[2].includes(':') ? p[2].split(':')[0] : 0) : obj.min);
												obj.max = ((obj.max == '') ? (p[2].includes(':') ? p[2].split(':')[1] : 1) : obj.max);
												templates.push(obj);
											});
										}
									}
								});
								if (isParameter == false) {
									if (templateSplited.length > 2) {
										if (templateSplited[2].includes('cc') || templateSplited[2].includes('note') || templateSplited[2].includes('pb')) {
											var audioOrMidi = 'midi';
										} else {
											var audioOrMidi = 'audio';
										}
										var obj = {
											type: audioOrMidi,
											name: templateSplited[0],
											templateFull: template,
											eventType: templateSplited.length > 2 ? (templateSplited[2].includes(':') ? templateSplited[2].split(':')[0] : templateSplited[2]) : '',
											eventName: templateSplited.length > 2 ? (templateSplited[2].includes(':') ? templateSplited[2].split(':')[1] : '') : '',
											options: templateSplited.length > 1 ? (templateSplited[1]) : '',
											array: templateSplited.length > 0 ? (templateSplited[0].includes(';') ? templateSplited[0].split(';') : '') : '',
											min: templateSplited.length > 0 ? (templateSplited[0].includes(':') ? templateSplited[0].split(':')[0] : '') : '',
											max: templateSplited.length > 0 ? (templateSplited[0].includes(':') ? templateSplited[0].split(':')[1] : '') : '',
										}
										templates.push(obj);
									}
								}
							}
						} else {
							templates.push({
								type: 'pure',
								name: '',
								templateFull: '',
								eventType: '',
								eventName: '',
								options: '',
								array: '',
								min: '',
								max: '',
							});
						}
						all.push({
							element: element.mSelectorText,
							pseudo: (element.mSelectorText.includes(':') ? true : false),
							property: css.property,
							value: css.valueText,
							templates: templates
						});
					});
				}
			});
		}
		chrome.storage.local.set({ all: all });
		if (result.debug)
			console.log(all);
	});
}


/**
 * @description get CSS and analyse it to create events.
 */
function updateCss() {
	let cssStr = editor.getValue();
	chrome.storage.local.set({ css: cssStr });
	analyseAll();
	sendMessage({ type: 'update', data: false });
}

/**
 * @description Activate CSS LSD.
 */
$('#onOff').on('click', function () {
	activateButton = !activateButton;
	chrome.storage.sync.set({ activate: activateButton }, function () {
		if (activateButton) {
			$('#onOff').addClass('active');
		} else {
			$('#onOff').removeClass('active');
		}
		chrome.storage.sync.set({ audioI: false });
		chrome.storage.sync.set({ midiI: false });
		sendMessage({ type: 'start', data: activateButton });
	});
});

/**
 * @description  Activate / desactivate audio button.
 */
$('.audioI').on('click', function () {
	audioButton = !audioButton;
	chrome.storage.sync.set({ audioI: audioButton });
	sendMessage({ type: 'audio', data: audioButton });
	initUi();
});

/**
 * @description Activate / desactivate midi button.
 */
$('.midiI').on('click', function () {
	midiButton = !midiButton;
	chrome.storage.sync.set({ midiI: midiButton });
	sendMessage({ type: 'midi', data: midiButton });
	initUi();
});

/**
 * @description Sensibility slider.
 */
$('#sensibility').on('change', function () {
	console.log('cc');
	chrome.storage.sync.set({ sensibility: $(this).val() });
});

/**
 * @description Duplicate the current row and save the parameters.
 */
$(document).on('click', '.icon-tabler-plus', function () {
	// Find parent row of the button and duplicate it
	var row = $(this).parent().parent();
	var newRow = row.clone();
	// Add the new row after the original one
	row.after(newRow);
	saveParameters();
});

/**
 * @description Remove a row, and save the parameters. If the row is the last one, add a new empty row.
 */
$(document).on('click', '.icon-tabler-minus', function () {
	var row = $(this).parent().parent();
	var table = row.parent();
	row.remove();
	saveParameters();
	if (table.children().length == 0) {
		table.append("<tr><td>" + icon + plus + minus + "</td><td contenteditable></td><td contenteditable></td><td contenteditable></td><td contenteditable></td><td contenteditable></td></tr>");
	}
});

/**
 * @description Send message to start/stop loop record with ID.
 */
var record = false;
$(document).on('click', '#record', function () {
	id = $(this).val();
	record = !record;
	if (record) {
		$('#record').html(stopIcon);
	}
	else {
		$('#record').html(recordIcon);
	}
	sendMessage({ bool: record, number: id });
});

/**
 * @description Send message to play loop with ID.
 */
$(document).on('click', '#loopPlay', function () {
	if ($(this).html().includes('player-play')) {
		$(this).html(pauseIcon);
		chrome.storage.sync.set({ loopPlay: true });
	} else {
		$(this).html(playIcon);
		chrome.storage.sync.set({ loopPlay: false });

	}
	id = $(this).val();
	sendMessage({ type: 'playLoop', data: id });
});

/**
 * @description Send message to reset loop with ID.
 */
$(document).on('click', '#loopReset', function () {
	id = $(this).val();
	sendMessage({ type: 'resetLoop', data: id });
});

/**
 * @description Get editor and parameters, transform them in JSON and download it.
 */
$('#download').on('click', function () {
	const filename = 'data.json';
	chrome.storage.local.get(['parameters'], function (result) {
		const jsonStr = JSON.stringify([editor.getValue(), result.parameters]);
		let element = document.createElement('a');
		element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(jsonStr));
		element.setAttribute('download', filename);
		element.style.display = 'none';
		document.body.appendChild(element);
		element.click();
		document.body.removeChild(element);
	});
});

/**
 * @description Load JSON file and update the editor and other parameters
 */
$('#upload').on('change', function () {
	const file = this.files[0];
	const reader = new FileReader();
	reader.onload = function (e) {
		const jsonStr = e.target.result;
		const jsonObj = JSON.parse(jsonStr);
		chrome.storage.local.set({ css: jsonObj[0] });
		chrome.storage.local.set({ parameters: jsonObj[1] });
		editor.setValue(jsonObj[0]);
		initUi();
	};
	reader.readAsText(file);
	this.value = "";
});

/**
 * @description Send message to the content script.
 * @param {dict} msg 
 */
function sendMessage(msg) {
	chrome.tabs.query({ active: true, currentWindow: true },
		function (tabs) {
			chrome.tabs.sendMessage(tabs[0].id, msg);
		}
	);
}

/**
 * Function trigger when new message received.
 * @param {type, data} message parameters 
 */
function onMessage({ type, data }) {
	console.log('onMessage', type, data);
	switch (type) {
		case 'updateUi': {
			initUi();
			break;
		}
		case 'midiEvent': {
			if (dataMidi != data) {
				$('#midiEvent').text(data);
				$('#midiEvent').parent().animate({ 'background-color': 'red' }, 'slow');
				$('#midiEvent').parent().animate({ 'background-color': 'transparent' }, 'slow');
				var focusedTd = $('[contenteditable]:focus');
				var focusedCol = $(focusedTd).index('#parameters tr:eq(' + $(focusedTd).parent().index('#parameters tr') + ') td');
				if (focusedTd.length > 0 && focusedCol == 4) {
					focused.text(focused.text() + ' ' + data);
				}
				dataMidi = data;
			}
			break;
		}
		case 'midiDevices': {
			$('#midiDevices').text(data);
			break;
		}
	}
}

// Wait messages from content script
chrome.runtime.onMessage.addListener(onMessage);
// Indicate to Background that the popup is ready
var port = chrome.runtime.connect({ name: "popup" });

// Load the editor
loadEditor();
// Initialize the settings UI
initUi();

$(document).tooltip({
	container: 'body',
	placement: 'top left',
	trigger: 'hover'
});
$(function () {
	$("#accordion").accordion({
		heightStyle: "content",
		collapsible: true,
		icons: {
			header: "ui-icon-closed",
			activeHeader: "ui-icon-open"
		}
	});
	$("#toggle").button().on("click", function () {
		if ($("#accordion").accordion("option", "icons")) {
			$("#accordion").accordion("option", "icons", null);
		} else {
			$("#accordion").accordion("option", "icons", icons);
		}
	});
});