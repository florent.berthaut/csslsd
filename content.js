var audioContext = null;
var meter = { volume: 0 };
var analyser = null;
var rafID = null;
var buflen = 1024;
var buf = new Float32Array(buflen);
var audio = false;
var midi = false;
var debug = false;
var audioSample = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

var sensibility;
chrome.storage.sync.get(['sensibility'], function (result) {
    sensibility = result.sensibility;
});

var parameterSave = {};
function range(template, value) {
    if (template.options.includes('i')) {
        increment = parseInt(template.options.split(':')[1]);
        if (template.name in parameterSave) {
            if (template.array != '') {
                parameterSave[template.name] = (parameterSave[template.name] + increment) > template.array.length ? 0 : (parameterSave[template.name] + increment);
                return template.array[parameterSave[template.name]]
            }
            else {
                if (increment > 0) {
                    parameterSave[template.name] = (parameterSave[template.name] + increment) > parseInt(template.max) ? parseInt(template.min) : (parameterSave[template.name] + increment);
                } else {
                    parameterSave[template.name] = (parameterSave[template.name] + increment) < parseInt(template.min) ? parseInt(template.max) : (parameterSave[template.name] + increment);
                }
                return parameterSave[template.name];
            }
        }
        else {
            parameterSave[template.name] = parseInt(template.min);
            return parseInt(template.min);
        }
    }
    else if (template.options == 'r') {
        if (template.array != '') {
            return template.array[Math.floor(Math.random() * template.array.length)];
        } else {
            if (template.min[0] == '#') {
                template.min = parseInt(template.min.replace('#', ''), 16);
                template.max = parseInt(template.max.replace('#', ''), 16);
                return '#' + ((Math.random() * (template.max - template.min) + template.min).toString(16)).split('.')[0];
            }
            else {
                return (Math.random() * (parseInt(template.max) - parseInt(template.min)) + parseInt(template.min));
            }
        }
    }
    else if (template.options == 'd') {
        if (template.min[0] == '#') {
            template.min = parseInt(template.min.replace('#', ''), 16);
            template.max = parseInt(template.max.replace('#', ''), 16);
            return '#' + ((template.min + Math.round(template.max - template.min) * value).toString(16)).split('.')[0];
        }
        else {
            return (parseInt(template.min) + Math.round(parseInt(template.max) - parseInt(template.min)) * value);
        }
    }
}

/**
 * Initialize Midi
 */
function midiApi() {
    chrome.storage.sync.get(['midiI', 'activate'], function (result) {
        midi = result.midiI;
        activate = result.activate;
        if (midi && activate) {
            WebMidi
                .enable()
                .then(launchMidi)
                .catch(err => alert(err));
        }
        else {
            WebMidi.disable();
            chrome.runtime.sendMessage({ type: 'updateUi', data: false });
        }
    });
}

/**
 * Launch Midi after being started by midiAPi()
 */
function launchMidi() {
    // Display available MIDI input devices
    if (WebMidi.inputs.length < 1) {
        chrome.storage.sync.set({ midiI: false });
        chrome.runtime.sendMessage({ type: 'updateUi', data: false });
        chrome.runtime.sendMessage({ type: 'midiDevices', data: 'Pas d\'appareil trouvé.' });
    } else {
        chrome.storage.sync.set({ midiI: true });
        chrome.runtime.sendMessage({ type: 'updateUi', data: true });

        devices = '';
        WebMidi.inputs.forEach((device, index) => {
            devices += index + ' : ' + device.name + device.manufacturer + '<br>';
        });
        chrome.runtime.sendMessage({ type: 'midiDevices', data: devices });

        const mySynth = WebMidi.inputs[0];
        mySynth.addListener("noteon", e => {
            midiEvent('noteon', e);
        }, { channels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16] });
        mySynth.addListener("controlchange", e => {
            midiEvent('controlchange', e);
        }, { channels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16] });
    }

}


/**
 * Trigger when midi is used. Change value between 0.0 and 1.0 
 * @param {str} type type of midi event
 * @param {dict} data event object
 */
function midiEvent(type, data) {
    chrome.storage.sync.get(['popup'], function (result) {
        if (result.popup) {
            switch (type) {
                case 'noteon':
                    chrome.runtime.sendMessage({ type: 'midiEvent', data: data.note.number });
                    break;
                case 'controlchange':
                    chrome.runtime.sendMessage({ type: 'midiEvent', data: data.controller.number });
                    break;
                case 'pitchbend':
                    chrome.runtime.sendMessage({ type: 'midiEvent', data: data.type });
                    break;
                default:
                    break;
            }
        }
    });
    chrome.storage.local.get(['all'], function (result) {
        result.all.forEach(element => {
            value = element.value;
            midiValue = data.value;
            eventIn = false;
            element.templates.forEach(template => {
                if (template.type == 'midi') {
                    midiValue = range(template, midiValue);
                    if (type == 'noteon' && template.eventType == 'note' && template.eventName == data.note.number) {
                        value = element.value.replace('[' + template.templateFull + ']', midiValue);
                        eventIn = true;
                    }
                    if (type == 'controlchange' && template.eventType == 'cc' && template.eventName == data.controller.number) {
                        value = element.value.replace('[' + template.templateFull + ']', midiValue);
                        eventIn = true;
                    }
                    if (type == 'pitchbend' && template.eventType == 'pb') {
                        value = element.value.replace('[' + template.templateFull + ']', midiValue);
                        eventIn = true;
                    }
                }
            });
            if (eventIn) {
                if (element.pseudo) {
                    root.style.setProperty("--pseudo-" + element.element.replaceAll(':', '') + '-' + element.property, value);
                }
                else {
                    let els = document.querySelectorAll(element.element);
                    els.forEach(e => {
                        e.style[element.property] = value;
                    });
                }
            }
        });
    });

    if (type == 'noteon') {
        for (let index = 0; index < INFOLOOP.length; index++) {
            const element = INFOLOOP[index];
            if (element.controllerPlayPause == data.note.number) {
                loop(index);
            }
            else if (element.controllerRecord == data.note.number) {
                INFOLOOP[index].isRecording = !INFOLOOP[index].isRecording;
                record(INFOLOOP[index].isRecording, index);
            }
        };
    }

    if (isRecording) {
        const time = Math.floor(performance.now() - recordingTime);
        RECORDED[recordCount].push({ type, data, time })
    }

}

/**
 * Initialize audio, request micro to user 
 */
function audioApi() {
    chrome.storage.sync.get(['audioI', 'activate'], function (result) {
        audio = result.audioI;
        activate = result.activate;
        if (audio && activate) {
            window.AudioContext = window.AudioContext || window.webkitAudioContext;
            audioContext = new AudioContext();
            try {
                navigator.getUserMedia =
                    navigator.getUserMedia ||
                    navigator.webkitGetUserMedia ||
                    navigator.mozGetUserMedia;
                navigator.getUserMedia(
                    {
                        "audio": {
                            "mandatory": {
                                "googEchoCancellation": "false",
                                "googAutoGainControl": "false",
                                "googNoiseSuppression": "false",
                                "googHighpassFilter": "false"
                            },
                            "optional": []
                        },
                    }, gotStream, didntGetStream);
            } catch (e) {
                alert('getUserMedia threw exception :' + e);
            }
        }
        else {
            if (debug)
                console.info('Audio suspend');
            if (window.streamReference) {
                audioContext.suspend();
                window.streamReference.getAudioTracks().forEach(function (track) {
                    track.stop();
                });
                window.streamReference.getVideoTracks().forEach(function (track) {
                    track.stop();
                });
                window.streamReference = null;
            }
            chrome.storage.sync.set({ audioI: false });
            audio = false;
            chrome.runtime.sendMessage({ type: 'updateUi', data: false });

        }
    });
};

function didntGetStream() {
    chrome.storage.sync.set({ audioI: false });
    audio = false;
    chrome.runtime.sendMessage({ type: 'updateUi', data: false });
    if (debug)
        console.error('Stream generation failed.');
}

var mediaStreamSource = null;
async function gotStream(stream) {
    // Create an AudioNode from the stream.
    mediaStreamSource = audioContext.createMediaStreamSource(stream);
    window.streamReference = stream;
    analyser = audioContext.createAnalyser();
    analyser.fftSize = 2048;
    analyser.maxDecibels = -25;
    analyser.minDecibels = -85;
    mediaStreamSource.connect(analyser);
    // Create a new volume meter and connect it.
    await audioContext.audioWorklet.addModule(chrome.runtime.getURL("worklet.js"));
    if (debug)
        console.log(chrome.runtime.getURL("worklet.js"));
    const node = new AudioWorkletNode(audioContext, 'vumeter');
    node.port.onmessage = event => {
        let _volume = 0
        if (event.data.volume)
            _volume = event.data.volume * sensibility;
        meter = { volume: _volume };
    }
    mediaStreamSource.connect(node);
    audioContext.resume();
    if (debug)
        console.info('Audio start');
    chrome.storage.sync.set({ audioI: true });
    chrome.runtime.sendMessage({ type: 'updateUi', data: true });
    // kick off the visual updating
    audioEvent();
}


var noteStrings = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"];

function noteFromPitch(frequency) {
    var noteNum = 12 * (Math.log(frequency / 440) / Math.log(2));
    return Math.round(noteNum) + 69;
}

function frequencyFromNoteNumber(note) {
    return 440 * Math.pow(2, (note - 69) / 12);
}

function centsOffFromPitch(frequency, note) {
    return Math.floor(1200 * Math.log(frequency / frequencyFromNoteNumber(note)) / Math.log(2));
}

var MIN_SAMPLES = 0;  // will be initialized when AudioContext is created.

function autoCorrelate(buf, sampleRate) {
    var SIZE = buf.length;
    var MAX_SAMPLES = Math.floor(SIZE / 2);
    var best_offset = -1;
    var best_correlation = 0;
    var rms = 0;
    var foundGoodCorrelation = false;
    var correlations = new Array(MAX_SAMPLES);

    for (var i = 0; i < SIZE; i++) {
        var val = buf[i];
        rms += val * val;
    }
    rms = Math.sqrt(rms / SIZE);
    if (rms < 0.01) // not enough signal
        return -1;

    var lastCorrelation = 1;
    for (var offset = MIN_SAMPLES; offset < MAX_SAMPLES; offset++) {
        var correlation = 0;

        for (var i = 0; i < MAX_SAMPLES; i++) {
            correlation += Math.abs((buf[i]) - (buf[i + offset]));
        }
        correlation = 1 - (correlation / MAX_SAMPLES);
        correlations[offset] = correlation; // store it, for the tweaking we need to do below.
        if ((correlation > 0.9) && (correlation > lastCorrelation)) {
            foundGoodCorrelation = true;
            if (correlation > best_correlation) {
                best_correlation = correlation;
                best_offset = offset;
            }
        } else if (foundGoodCorrelation) {
            var shift = (correlations[best_offset + 1] - correlations[best_offset - 1]) / correlations[best_offset];
            return sampleRate / (best_offset + (8 * shift));
        }
        lastCorrelation = correlation;
    }
    if (best_correlation > 0.01) {
        return sampleRate / best_offset;
    }
    return -1;
}

function freqToBin(freq, rounding = 'round') {
    const max = analyser.frequencyBinCount - 1,
        bin = Math[rounding](freq * 256 / audioContext.sampleRate);

    return bin < max ? bin : max;
}

// Allow to store the current note in an index, for duration computation
chrome.storage.local.get(['records'], function (result) {
    RECORDED = result.records.loop;
    INFOLOOP = result.records.info;
});

let recordCount = 0;
let isRecording = false;
let recordingTime = 0;
let theLoop;
const record = (status, number) => {
    INFOLOOP[number].isRecording = !INFOLOOP[number].isRecording;
    isRecording = status;
    if (status) {
        recordingTime = performance.now();
        recordCount = number;
    }
    chrome.storage.local.set({ 'records': { loop: RECORDED, info: INFOLOOP } });
};


// Start loop
const loop = (number) => {
    INFOLOOP[number].isLoop = !INFOLOOP[number].isLoop;
    chrome.storage.local.set({ 'records': { loop: RECORDED, info: INFOLOOP } });
    isRecording = false;
    if (RECORDED[number].length) {
        const loopLength = RECORDED[number][RECORDED[number].length - 1].time;
        if (INFOLOOP[number].isLoop) {
            loopNotes(number);
            theLoop = setInterval(() => loopNotes(number), loopLength);
        } else {
            clearInterval(theLoop)
        }
    }
};

const loopNotes = (number) => {
    RECORDED[number].forEach(note => {
        setTimeout(() => {
            if (!INFOLOOP[number].isLoop) return;
            midiEvent(note.type, note.data)
        }, note.time);
    })
}

const reset = (number) => {
    RECORDED[number].length = 0;
    isRecording = false;
    isLoop = false;
    chrome.storage.local.set({ 'records': { loop: RECORDED, info: INFOLOOP } });
}
const presets = {
    low: [20, 250],
    lomi: [250, 500],
    mid: [500, 2e3],
    mihi: [2e3, 4e3],
    hi: [4e3, 16e3]
}
function audioEvent() {
    analyser.getFloatTimeDomainData(buf);
    var ac = autoCorrelate(buf, audioContext.sampleRate);
    if (ac != -1) {
        var note = noteFromPitch(ac);
        var detune = centsOffFromPitch(ac, note);
    }
    var bufferLength = analyser.frequencyBinCount;
    var dataArray = new Uint8Array(bufferLength);
    analyser.getByteFrequencyData(dataArray);

    var startFreq, endFreq, startBin, endBin, energy;

    energies = [];
    Object.keys(presets).forEach(key => {
        [startFreq, endFreq] = presets[key];
        startBin = freqToBin(startFreq);
        endBin = endFreq ? freqToBin(endFreq) : startBin;
        energy = 0;
        for (let i = startBin; i <= endBin; i++)
            energy += dataArray[i];
        energies[key] = energy / (endBin - startBin + 1) / 255;
    });

    const sum = audioSample.reduce((a, b) => a + b, 0);
    const avg = (sum / audioSample.length) || 0;
    var attack = false;
    if (avg + 0.05 < meter.volume) {
        attack = true;
    }
    audioSample.shift();
    audioSample.push(meter.volume);
    chrome.storage.local.get(['all'], function (result) {
        if (result.all.length > 0) {
            result.all.forEach(element => {
                value = element.value;
                eventIn = false;
                element.templates.forEach(template => {
                    if (template.type == 'audio') {
                        Object.keys(presets).forEach(preset => {
                            if (template.eventType == preset) {
                                eventIn = true;
                                value = value.replace('[' + template.templateFull + ']', range(template, energies[preset]));
                            }
                        });
                        if (template.eventType == 'loud') {
                            eventIn = true;
                            volume = meter.volume * 1.4;
                            value = value.replace('[' + template.templateFull + ']', (meter.volume * 1.4 < 1 ? range(template, volume) : 0));
                        }
                        else if (template.eventType == 'onset' && template.eventName == noteStrings[note % 12] && meter.volume > 0.08) {
                            eventIn = true;
                            value = value.replace('[' + template.templateFull + ']', range(template, 1));
                        }
                        else if (attack && template.eventType == 'attack') {
                            if (debug)
                                console.log('Attack');
                            eventIn = true;
                            value = value.replace('[' + template.templateFull + ']', '');
                        }
                    }
                });
                if (eventIn) {
                    if (element.pseudo) {
                        root.style.setProperty("--pseudo-" + element.element.replaceAll(':', '') + '-' + element.property, value);
                    }
                    else {
                        let els = document.querySelectorAll(element.element);
                        els.forEach(e => {
                            e.style[element.property] = value;
                        });
                    }
                }
            });
        }
    });

    if (audio) {
        rafID = window.requestAnimationFrame(audioEvent);
    }
}

function pureCss() {
    chrome.storage.local.get(['all'], function (result) {
        if (result.all.length > 0) {
            const elements = document.getElementsByClassName('CSSLSD');
            while (elements.length > 0) {
                elements[0].parentNode.removeChild(elements[0]);
            }
            result.all.forEach(element => {
                element.templates.forEach(template => {
                    if (template.type == 'pure' || element.pseudo) {
                        if (element.element.includes(':after') || element.element.includes(':before')) {
                            if (template.type != 'pure') {
                                var elemDiv = document.createElement('style');
                                elemDiv.innerText = ':root { --pseudo-' + element.element.replaceAll(':', '') + '-' + element.property + ' : "";}';
                                elemDiv.className = 'CSSLSD';
                                document.body.appendChild(elemDiv);
                                var elemDiv = document.createElement('style');
                                elemDiv.innerText = element.element + '{' + element.property + ': var(--pseudo-' + element.element.replaceAll(':', '') + '-' + element.property + ');}';
                                elemDiv.className = 'CSSLSD';
                                document.body.appendChild(elemDiv);
                            }
                            else {
                                var elemDiv = document.createElement('style');
                                elemDiv.innerText = element.element + '{' + element.property + ':' + element.value + ';}';
                                elemDiv.className = 'CSSLSD';
                                document.body.appendChild(elemDiv);
                            }
                        }
                        else {
                            let els = document.querySelectorAll(element.element);
                            els.forEach(e => {
                                e.style[element.property] = element.value;
                            });
                        }
                    }
                });
            });
        }
    });
}

/**
 * Function trigger when new message received.
 * @param {type, data} message parameters 
 */
function onMessage({ type, data }) {
    if (debug)
        console.info('Message received from CSSLSD back :', type, data);
    switch (type) {
        case 'update': {
            chrome.storage.sync.get(['activate'], function (result) {
                if (result.activate) {
                    pureCss();
                }
            });
            break;
        }
        case 'start': {
            chrome.storage.sync.get(['activate'], function (result) {
                if (result.activate) {
                    pureCss();
                }
                else {
                    audioApi();
                    midiApi();
                }
            });
            break;
        }
        case 'audio': {
            audioApi();
            break;
        }
        case 'midi': {
            midiApi();
            break;
        }
        case 'playLoop': {
            chrome.storage.sync.get(['activate'], function (result) {
                if (result.activate) {
                    loop(data);
                }
            });
            break;
        }
        case 'record': {
            record(data.bool, data.number);
            break;
        }
        case 'resetLoop': {
            reset(data);
            chrome.runtime.sendMessage({ type: 'updateUi', data: false });
            break;
        }
        case 'debug': {
            if (debug) {
                chrome.storage.sync.set({ debug: false });
                debug = false;
            }
            else {
                chrome.storage.sync.set({ debug: true });
                debug = true;
            }
            break;
        }
    }
}


chrome.storage.sync.set({ popup: true });
chrome.runtime.onMessage.addListener(onMessage);
chrome.storage.sync.get(['activate', 'debug'], function (result) {
    if (result.activate) {
        audioApi();
        midiApi();
    }
    debug = result.debug;
});
