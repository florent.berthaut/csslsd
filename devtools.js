(function () {

    var backgroundPageConnection = chrome.runtime.connect({
        name: "devtools-page"
    });

    backgroundPageConnection.onMessage.addListener(function (message) {
        // Handle responses from the background page, if any
    });

    var panels = chrome.devtools.panels;

    // panel
    var panel = panels.create(
        "CSSLSD",
        "ressources/icon256.png",
        "popup.html", function () {
            // Relay the tab ID to the background page
        }
    );
})();